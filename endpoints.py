__author__ = 'Khoir MS'

import urllib
import urlparse

class Endpoints:

    __BASE_URL = 'https://www.googleapis.com'
    __API_URL = 'https://www.googleapis.com/youtube/v3'
    __CHANNEL_SEARCH = '{api_url}/search?part=snippet&q={query}&maxResults={max_result}&order=relevance&type=channel&pageToken={page_token}&access_token={access_token}&key={apikey}'
    __CHANNEL_BY_ID_INFO = '{api_url}/channels?part=snippet,brandingSettings,statistics&id={id}&access_token={access_token}&key={apikey}'
    __CHANNEL_BY_USERNAME_INFO = '{api_url}/channels?part=snippet,brandingSettings,statistics?forUsername={username}&access_token={access_token}&key={apikey}'

    __VIDEOS = '{api_url}/search?part=snippet&channelId={id}&q={query}&location={location}&locationRadius={radius}&publishedAfter={published_after}&maxResults={max_result}&order=date&type=video&pageToken={page_token}&access_token={access_token}&key={apikey}'
    __VIDEO_DETAIL = '{api_url}/videos?part=snippet,statistics,recordingDetails&id={id}&access_token={access_token}&key={apikey}'
    __COMMENT_LIST = '{api_url}/commentThreads?part=snippet&videoId={id}&maxResults={max_result}&order=time&nextPageToken={page_token}&access_token={access_token}&key={apikey}'
    __COMMENT_DETAIL = '{api_url}/comments?part=snippet&id={id}&access_token={access_token}&key={apikey}'

    def __init__(self):
        pass

    @staticmethod
    def search_channel(query, max_result=50, page_token='', access_token='', apikey=''):
        if max_result < 1 or max_result > 50: max_result = 50
        endpoint = Endpoints.__CHANNEL_SEARCH.format(api_url=Endpoints.__API_URL, query=query, max_result=max_result, page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def channel_info(id=None, username=None, access_token='', apikey=''):
        if id:
            endpoint = Endpoints.__CHANNEL_BY_ID_INFO.format(api_url=Endpoints.__API_URL, id=id, access_token=access_token, apikey=apikey)
        else:
            endpoint = Endpoints.__CHANNEL_BY_USERNAME_INFO.format(api_url=Endpoints.__API_URL, username=username, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def videos(channel_id='', query='', location='', location_radius='', published_after='', max_result=50, page_token='', access_token='', apikey=''):
        if query: query = '"{}"'.format(query)
        if max_result < 1 or max_result > 50: max_result = 50

        endpoint = Endpoints.__VIDEOS.format(api_url=Endpoints.__API_URL, id=channel_id, query=query, location=location, radius=location_radius,
            published_after=published_after, max_result=max_result, page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def video_detail(video_id, access_token='', apikey=''):
        # vid1,vid2,vidn, if multiple video id
        endpoint = Endpoints.__VIDEO_DETAIL.format(api_url=Endpoints.__API_URL, id=video_id, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def comments(video_id, max_result=100, page_token='', access_token='', apikey=''):
        if max_result < 1 or max_result > 100: max_result = 100
        endpoint = Endpoints.__COMMENT_LIST.format(api_url=Endpoints.__API_URL, id=video_id, max_result=max_result,
                                                   page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def comment_detail(comment_id, access_token='', apikey=''):
        # cid1,cid2,cidn, if multiple comment id
        endpoint = Endpoints.__COMMENT_DETAIL.format(api_url=Endpoints.__API_URL, id=comment_id, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def parse_qstring(url):
        return dict(urlparse.parse_qsl(urlparse.urlsplit(url).query))

if __name__=='__main__':
    print Endpoints.search_channel(query="dede", apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.channel_info(id="UCJXavYQrHc3a5MQVXSnJhpQ", apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.videos(channel_id="UCJXavYQrHc3a5MQVXSnJhpQ", apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.videos(query="memory", apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.videos(location="-6.918441,107.625988", apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.video_detail(video_id='eszV2-0IGrU', apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.comments(video_id='eszV2-0IGrU', apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.comment_detail(comment_id='z12kst1ihuigi3ts523gzfebgzyisfbxh04', apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')

    # print Endpoints.video_by_keyword(query="memory", apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
    # print Endpoints.people_info(user_id="dede", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    # print Endpoints.activities(user_id="1234", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    # print Endpoints.activity_detail(activity_id="1234", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    # print Endpoints.search_activity(query="dede", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
