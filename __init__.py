__author__ = 'Khoir MS'

import re
from browser import Browser
from endpoints import Endpoints

class Youtube(Browser):

    def __init__(self, **kwargs):
        super(Youtube, self).__init__(**kwargs)

        self.__apikey = ''
        self.__access_token = ''
        self.is_credential_set = False
        self.response = None

    def set_credential(self, apikey='', access_token=''):
        self.log('setup credential key ...')
        self.__apikey = apikey
        self.__access_token = access_token

    def search_channel(self, query, max_result=50, page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.search_channel(query=query, max_result=max_result,
                                           page_token=page_token, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def channel_info(self, channel_id=None, username=None, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        if not (channel_id or username):
            raise Exception("user id or username is not defined")

        self.__is_credential_set()
        endpoint = Endpoints.channel_info(id=channel_id, username=username, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def videos(self, channel_id='', query='', location='', location_radius='', published_after='', max_result=50, page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())
        # location format latitude,longitude, ex: 37.42307,-122.08427
        if location and not location_radius: location_radius = '10km'
        # The value of published_after is an RFC 3339 formatted date-time value(1970-01-01T00:00:00Z)
        if published_after:
            published_after = published_after.strip()
            published_after = re.sub(r'^(\d{4}-\d{2}-\d{2})$', '\g<1>T00:00:00Z', published_after)
            if not re.search(r'^\d{4}-\d{2}-\d{2}T00:00:00Z$', published_after):
                raise Exception('The value of published_after must an RFC 3339 formatted date-time value(1970-01-01T00:00:00Z)')
        self.__is_credential_set()
        endpoint = Endpoints.videos(channel_id=channel_id, query=query, location=location, location_radius=location_radius,
                                    published_after=published_after, max_result=max_result, page_token=page_token,
                                    apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def video_detail(self, video_id, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())
        if isinstance(video_id, list):
            video_id = ','.join(video_id)

        self.__is_credential_set()
        endpoint = Endpoints.video_detail(video_id=video_id, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def comments(self, video_id, max_result=100, page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.comments(video_id=video_id, max_result=max_result,
                                      page_token=page_token, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def comment_detail(self, comment_id, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.comment_detail(comment_id=comment_id, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def __is_credential_set(self):
        if not (self.__apikey or self.__access_token):
            raise Exception('please set credential key using apikey or access_token')

        self.is_credential_set = True

    def get_api_errors(self):
        eresponse = self.response.json()
        ecode = eresponse['error']['code']
        emessage = eresponse['error']['message']

        if ecode == 403:
            reason = eresponse['error']['errors'][0]['reason']
            if re.search(r'dailyLimitExceeded', reason, flags=re.I):
                raise DailyLimitExcedeed(emessage)

        if ecode == 404:
            reason = eresponse['error']['errors'][0]['reason']
            if re.search(r'notFound', reason, flags=re.I):
                raise ChannelNotExists(emessage)

        if ecode != 200:
            raise BaseApiException(emessage)

class DailyLimitExcedeed(Exception):
    pass

class ChannelNotExists(Exception):
    pass

class BaseApiException(Exception):
    pass

if __name__=="__main__":
    import json
    yt = Youtube()

    try:
        yt.set_credential(apikey='AIzaSyBnEEX3BaSlziNvhw4At_u1c8ez5FRsN7c')
        # yt.set_credential(apikey='AIzaSyDZHXvJ6ho629F10NbP7_N_ySY35K09Nls')
        # data = gp.search_channel(query='dede')
        # data = yt.channel_info(channel_id='UC087mmPVZ29WJCpYWFNBn1g')
        # video by channel
        # data = yt.videos(channel_id='UC087mmPVZ29WJCpYWFNBn1g', published_after='2014-01-01')
        # video by keyword
        data = yt.videos(query='memory', published_after='2014-07-03')
        # video by location
        # data = yt.videos(location='-6.917418,107.621525')
        # video by custom
        # data = yt.videos(channel_id='UCTmhIy-IeUmBCrJ9trcPW7A', query='PENAMPAKAN', location='-6.917418,107.621525')
        # single video detail
        # data = yt.video_detail(video_id='E1STTNbVIWA')
        # multiple video detail
        # data = yt.video_detail(video_id=['shiDSwIT5ro', 'E1STTNbVIWA', 'OEmx1T3PqNI', 'hlq5DQj0ifE'])
        # data = yt.comments(video_id='Sbe56BoIAMg', max_result=1)
        # data = yt.comment_detail(comment_id='z12wfxxpfyeke3uxe04cgtzhjs3dzbcivio')

        print json.dumps(data, indent=4)
    except ChannelNotExists:
        pass
    except Exception as e:
        print e